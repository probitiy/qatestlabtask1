import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by alexander on 19/11/2017.
 */
public class Launcher {
    public static void main(String[] args) {
        firstTaskImplemntation();
        secondTaskImplemntation();
    }

    public static void firstTaskImplemntation() {
        WebDriver driver = initChromeDriver();
        adminLogin(driver);
        WebElement employeeIcon = driver.findElement(By.id("employee_infos"));
        employeeIcon.click();
        WebElement LogoutLink = driver.findElement(By.id("header_logout"));
        LogoutLink.click();
        waitTwoSeconds(driver);
        driver.quit();
    };

    public static void secondTaskImplemntation() {
        WebDriver driver = initChromeDriver();
        adminLogin(driver);
        List<WebElement> menuElements = driver.findElements(By.className("maintab"));
        List<String> menuElementHeaders = new ArrayList<String>();
        for(WebElement elem: menuElements) {
            menuElementHeaders.add(elem.getText());
        }
        for(String elemName: menuElementHeaders) {
            WebElement currentElement = driver.findElement(By.linkText(elemName));
            currentElement.click();
            System.out.println(elemName);
            String menuPageAddress = driver.getCurrentUrl();
            waitTwoSeconds(driver);
            driver.navigate().refresh();
            waitTwoSeconds(driver);
            String refreshPageAddress = driver.getCurrentUrl();
            if (menuPageAddress.equals(refreshPageAddress)) {
                System.out.println(elemName + " menu stays on the same address after refresh");
            } else {
                System.out.println(elemName + " menu changes address after refresh");
            }
        }
        driver.quit();
    }

    public static void adminLogin(WebDriver driver) {
        String initialPageLink = "http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/index.php?controller=AdminLogin&token=d251f363cceb5a849cb7330938c64dea";
        String email = "webinar.test@gmail.com";
        String password = "Xcg7299bnSmMuRLp9ITw";
        driver.get(initialPageLink);
        WebElement emailElement = driver.findElement(By.id("email"));
        emailElement.sendKeys(email);
        WebElement passwordElement = driver.findElement(By.id("passwd"));
        passwordElement.sendKeys(password);
        WebElement loginButton = driver.findElement(By.name("submitLogin"));
        loginButton.click();
        waitTwoSeconds(driver);
    }

    public static void waitTwoSeconds(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    public static WebDriver initChromeDriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver");
        return new ChromeDriver();
    }
}
